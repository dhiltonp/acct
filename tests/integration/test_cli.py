import os

from pytest_idem.runner import named_tempfile

ACCT_FILE = """
acct-backends:
  provider:
    backend_profile:
      kwarg1: val1
provider.test:
  profile1: <overridden by backend>
  profile3: null
"""


def test_decrypt(idem_cli):
    with named_tempfile(suffix=".yaml") as fh:
        fh.write_text(ACCT_FILE)

        ret = idem_cli("decrypt", fh)
        assert ret.json == {
            "acct-backends": {"provider": {"backend_profile": {"kwarg1": "val1"}}},
            "provider.test": {
                "profile1": {"kwarg1": "val1"},
                "profile2": {},
                "profile3": None,
            },
        }


def test_encrypt(idem_cli):
    with named_tempfile(suffix=".yaml") as fh:
        fh.write_text(ACCT_FILE)

        ret = idem_cli("encrypt", fh)

        assert ret.result, ret.stderr
        # Verify that an acct_key was generated
        assert ret.stdout, ret.stderr


def test_encrypt_decrypt(idem_cli):
    acct_key = "Vd9GeEklCb1bmSwTx3Edv7-QZGX9Urcdef-T8E_WeN4="
    with named_tempfile(suffix=".yaml") as fh:
        fh.write_text(ACCT_FILE)

        ret = idem_cli("encrypt", fh, f'--acct-key="{acct_key}"')
        assert ret.result, ret.stderr

    with open(f"{fh}.fernet") as fh:
        contents = fh.read()
        os.remove(fh.name)

    with named_tempfile(suffix=".yaml.fernet") as fh:
        fh.write_text(contents)

        ret = idem_cli("decrypt", fh, f'--acct-key="{acct_key}"')
        assert ret.result, ret.stderr
        assert ret.json == {
            "acct-backends": {"provider": {"backend_profile": {"kwarg1": "val1"}}},
            "provider.test": {
                "profile1": {"kwarg1": "val1"},
                "profile2": {},
                "profile3": None,
            },
        }
