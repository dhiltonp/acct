async def gather(hub, profiles):
    ret = {}

    for profile, ctx in profiles.get("provider", {}).items():
        ctx["processed"] = True
        ret[profile] = ctx

    return ret
